package com.boxever.optima

import com.boxever.optima.spec.mapper.estimators.ColumnTransformEstimator
import com.boxever.optima.spec.mapper.transform.{DataExtensionTransformer, ValidatedDataTransformer}
import com.boxever.labs.spark.optima.spec.{Mapping, OptimaSpec}
import org.apache.spark.ml.{Pipeline, PipelineStage}
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.storage.StorageLevel

object TransformPOC {

  def run(config: OptimaSpec, inputDataFrame: () => DataFrame, output: (DataFrame, DataFrame, Option[DataFrame]) => Unit): Unit = {

    val dexMappings = config.mappings.find(_.name == "dataExtensions")

    val stages: Seq[PipelineStage] = config.mappings.map(mappingToStage)

    val pipeline = new Pipeline()

    pipeline.setStages(( stages ++ Seq(new ValidatedDataTransformer()) ).toArray)

    val data = inputDataFrame().persist;
    val transformedData = pipeline.fit(data)


      .transform(data)
      .withColumn("s", size(col(ValidatedDataTransformer.columnName))).persist
    transformedData.explain(true)

    val validTransformedDF = transformedData.filter(s"size(${ValidatedDataTransformer.columnName}) = 0")

    // Get Invalid Records
    val quarantinedRecordsDf = transformedData.select("ref",ValidatedDataTransformer.columnName,"s").filter(s"size(${ValidatedDataTransformer.columnName}) > 0")
      .persist(StorageLevel.MEMORY_AND_DISK)


    val quarantinedSummary = quarantinedRecordsDf
      .groupBy("validationErrors")
      .count()
      .orderBy(desc("count"))
      .select(concat_ws("", col("validationErrors"))
                .as("validationErrors"), col("count"))

    output(validTransformedDF, quarantinedRecordsDf, None)

    data.unpersist()
    transformedData.unpersist()
    quarantinedRecordsDf.unpersist()

  }

  private def mappingToStage( m: Mapping ): PipelineStage = {
    val inputCol = m.source.name
    val outputCol = m.destination.name

    new ColumnTransformEstimator().setSpecParams(m).setInputCol(inputCol).setOutputCol(outputCol)
    // TODO: Specs carry input and output col, remove extra assigment by assing inside spec setter


  }

  private def respecDataExtensions( mappings: List[Mapping] ): List[Mapping] = {
    val dexMappings: List[Mapping] = mappings.filter(_.name == "dataExtensions").head.destination.mappings

    mappings.filter(_.name != "dataExtensions") ++ dexMappings

  }


  // Way slower. needs different mappings
  def runWithDex(config: OptimaSpec, inputDataFrame: () => DataFrame, output: (DataFrame, DataFrame, Option[DataFrame]) => Unit): Unit = {

    val dexMappings = config.mappings.find(_.name == "dataExtensions")

    val stages: Seq[PipelineStage] = if(dexMappings.isDefined ) {
      Seq( new DataExtensionTransformer().setSpecParams(dexMappings.get) ) ++ respecDataExtensions(config.mappings).map(mappingToStage)
    } else {
      config.mappings.map(mappingToStage)
    }

    val pipeline = new Pipeline()

    pipeline.setStages(( stages ++ Seq(new ValidatedDataTransformer()) ).toArray)

    val data = inputDataFrame().persist;
    val transformedData = pipeline.fit(data)


      .transform(data)
      .withColumn("s", size(col(ValidatedDataTransformer.columnName))).persist
    transformedData.explain(true)

    val validTransformedDF = transformedData.filter(s"size(${ValidatedDataTransformer.columnName}) = 0")

    // Get Invalid Records
    val quarantinedRecordsDf = transformedData.select("ref",ValidatedDataTransformer.columnName,"s").filter(s"size(${ValidatedDataTransformer.columnName}) > 0")
      .persist(StorageLevel.MEMORY_AND_DISK)


    val quarantinedSummary = quarantinedRecordsDf
      .groupBy("validationErrors")
      .count()
      .orderBy(desc("count"))
      .select(concat_ws("", col("validationErrors"))
                .as("validationErrors"), col("count"))

    output(validTransformedDF, quarantinedRecordsDf, None)

    data.unpersist()
    transformedData.unpersist()
    quarantinedRecordsDf.unpersist()

  }



}
