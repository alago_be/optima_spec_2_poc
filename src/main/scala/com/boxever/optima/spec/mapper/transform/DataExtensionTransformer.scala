package com.boxever.optima.spec.mapper.transform

import com.boxever.labs.spark.optima.complexstructure.ComplexStructureDestinationSchema.getSourceSchema
import com.boxever.labs.spark.optima.spec.Mapping
import com.boxever.optima.spec.mapper.estimators.HasSpecParams
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.ml.Model
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.sql.functions.{arrays_zip, col, expr, map_from_arrays, struct}
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Dataset, Column}

class DataExtensionTransformer(override val uid: String) extends Model[DataExtensionTransformer] with HasSpecParams {

  def this() = this(Identifiable.randomUID("DataExtensionTransformer"))

  override def copy(extra: ParamMap): DataExtensionTransformer = defaultCopy(extra)

  def setSpecParams(value: Mapping): this.type = set(specParams, value)

  // This works in spark-shell
  //.withColumn("dexNames",col("dataExtensions.name"))
  //.withColumn("dexValues", col("dataExtensions.values"))
  // .withColumn("dexes", map_from_arrays(col("dexNames"), col("dexValues")))
  // .drop("dexValues").drop("dexNames").drop("dataExtensions")
  // .withColumn("Weather", col("dexes")("Weather"))
  // .withColumn("Locations", col("dexes")("Locations")).drop("dexes").explain(true)
  override def transform(dataset: Dataset[_]): DataFrame = {
    val mappings: Mapping = getSpecParams
    val dexes: List[(String, StructType)] = mappings.destination.mappings.map(
      m =>
        (m.source.name,getSourceSchema(m.destination.mappings))
      )


    val dfWithMap = dataset.withColumn("dexNames",col("dataExtensions.name"))
    .withColumn("dexValues", col("dataExtensions.values"))
    .withColumn("dexes", map_from_arrays(col("dexNames"), col("dexValues")))
    .drop("dexValues").drop("dexNames").drop("dataExtensions")

    dexes.foldLeft(dfWithMap)(
      (df, dex) =>
        df.withColumn(dex._1, getColumnWithFields( col("dexes")(dex._1), dex._2) )
    ).drop("dexes")

//    val dfWithMap = dataset
//      .withColumn("dexes", arrays_zip(col("dataExtensions.name"), col("dataExtensions.values")))
//      .withColumn("dexes",  expr(  ))
//        .drop("dataExtensions")


  }

  private def getColumnWithFields(c: Column, columnSchema: StructType): Column = {
    struct( columnSchema.fields.map(s => c.getField(s.name)):_* ).cast(columnSchema)
  }

  override def transformSchema(schema: StructType): StructType = {
    val mappings: Mapping = getSpecParams
    val dexesSchemas = getSourceSchema(mappings.destination.mappings)

    StructType( schema.filter( _.name != "dataExtensions" ) ++ dexesSchemas )
  }
}

