package com.boxever.optima.spec.mapper

import scala.util.{Failure, Success, Try}

package object util {

  object Extensions {
    implicit class RichEither[L <: Throwable,R](e:Either[L,R]){
      def toTry:Try[R] = e.fold(Failure(_), Success(_))
    }

    implicit class RichTry[T](t:Try[T]){
      def toEither:Either[String,T] = t.transform(s => Success(Right(s)), f => Success(Left(f.getMessage))).get
    }
  }

}
