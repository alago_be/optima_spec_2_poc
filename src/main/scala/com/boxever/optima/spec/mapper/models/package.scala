package com.boxever.optima.spec.mapper

import com.boxever.labs.spark.optima.spec.Mapping
import org.apache.spark.ml.param.{IntParam, Param, ParamValidators, Params}
import org.apache.spark.sql.types.DataType

package object models {

  object TransformerTypes extends Enumeration {
    val Identity, Validator, Transformer, ValidatorAndTransformer = Value
  }


  class OutputTypeParam(parent: Params, name: String, doc: String, isValid: DataType => Boolean )
    extends Param[DataType](parent, name, doc, isValid) {

    def this(parent: Params, name: String, doc: String) =
      this(parent, name, doc, _ => true)
  }

  trait HasOutputTypeParam extends Params {

    final val outputType: OutputTypeParam =
      new OutputTypeParam(this, "specParams", "")

    def getSpecParams: DataType = $(outputType)
  }

  // The map curried ( or fitted) methods for every transformer/validator
  // Todo: Choose better names
  type FittedMethod = Any => Either[String, Any]
  type FittedMethods = Map[String, FittedMethod]
  type MethodWithParams = (Any, Seq[String]) => Either[String, Any]
  type MapOfMethodsWithParams = Map[String, MethodWithParams]

  def fitMethods(m: Mapping, mapOfMethods: MapOfMethodsWithParams): FittedMethods = {
    m.validations.map {
                        v => ( v.name, (x: Any) => mapOfMethods(v.name)(x, v.params.getOrElse(List()) ))
                      }.toMap
  }

  class ArrayLevel(parent: Params, name: String, doc: String, isValid: Int => Boolean )
    extends IntParam(parent, name, doc, isValid) {

    def this(parent: Params, name: String, doc: String) =
      this(parent, name, doc, ParamValidators.gtEq(0))
  }

  trait HasArrayLevelParams extends Params {
    final val arrayLevel: ArrayLevel =
      new ArrayLevel(this, "arrayLevel", "")

    def getArrayLevel: Int = $(arrayLevel)

  }

}
