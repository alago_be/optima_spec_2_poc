package com.boxever.optima.spec.mapper.validations

import com.boxever.labs.spark.optima.spec.Mapping
import com.boxever.optima.spec.mapper.models.{BaseColumnTransformer, FittedMethod, HasArrayLevelParams}
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.sql.{Column, DataFrame, Dataset}
import org.apache.spark.sql.functions.{col, expr, udf}
import org.apache.spark.sql.types.{StringType, StructType}

import scala.collection.mutable
import scala.reflect.runtime.{universe => ru}

trait Validator {
  def validate(dataset: Dataset[_]): Dataset[_]
}


trait ColumnValidator extends BaseColumnTransformer with HasValidationParams{
  self: Validator =>

  override def transformSchema(schema: StructType): StructType = {
    schema.add("validationErrors"+$(outputCol), StringType)
  }
}



class SqlColumnValidator(override val uid: String) extends ColumnValidator with Validator with HasSQLValidationParams  with HasArrayLevelParams{

  def this() = this(Identifiable.randomUID("SqlColumnValidator"))

  override def transform(dataset: Dataset[_]): DataFrame = {
    validate(dataset).toDF()
  }

  def validate(dataset: Dataset[_]): Dataset[_] = {
    val func: Column = $(sqlValidationParams)
    dataset.withColumn("validationErrors"+$(outputCol),func)
  }

  def setArrayLevel(value: Int): this.type = set(arrayLevel, value)

  setDefault(arrayLevel, 0)
}




