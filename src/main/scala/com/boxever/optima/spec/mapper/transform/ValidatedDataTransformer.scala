package com.boxever.optima.spec.mapper.transform

import com.boxever.optima.spec.mapper.models.BaseColumnTransformer
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.sql.{DataFrame, Dataset}
import org.apache.spark.sql.functions.{col, concat, typedLit}
import org.apache.spark.sql.types.{ArrayType, StringType, StructType}


class ValidatedDataTransformer(override val uid: String) extends BaseColumnTransformer {

  override def copy(extra: ParamMap): BaseColumnTransformer = defaultCopy(extra)

  def this() = this(Identifiable.randomUID("ValidatedDataTransformer"))

  override def transform(dataset: Dataset[_]): DataFrame = {
    val columns = dataset.columns.filter(_.startsWith(ValidatedDataTransformer.columnName))
    if(columns.length >0 ) {
      dataset.withColumn(ValidatedDataTransformer.columnName,
                         concat(columns.map(col): _*))
        .drop(columns: _*)
    }
    else  {
      dataset.withColumn(ValidatedDataTransformer.columnName, typedLit(Seq()))
      }
    }

  override  def transformSchema(schema: StructType): StructType = {
    StructType(schema.filter(!_.name.startsWith(ValidatedDataTransformer.columnName))).add(ValidatedDataTransformer.columnName, ArrayType(StringType))
    }

}

object ValidatedDataTransformer {
  val columnName = "validationErrors"
}