package com.boxever.optima.spec.mapper.estimators

import com.boxever.labs.spark.optima.spec.Mapping
import com.boxever.optima.spec.mapper.models._
import com.boxever.optima.spec.mapper.transform._
import com.boxever.optima.spec.mapper.validations.SqlColumnValidator
import org.apache.spark.ml.Estimator
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.ml.param.shared.{HasInputCol, HasOutputCol}
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.types._

class ColumnTransformEstimator(override val uid: String)
  extends Estimator[BaseColumnTransformer] with HasInputCol with HasOutputCol
    with HasSpecParams with HasArrayLevelParams {

  import ColumnTransformEstimator._

  def this() = this(Identifiable.randomUID("ColumnTransformEstimator"))

  def setInputCol(value: String): this.type = set(inputCol, value)
  def setOutputCol(value: String): this.type = set(outputCol, value)
  def setSpecParams(value: Mapping): this.type = set(specParams, value)
  def setArrayLevel(value: Int): this.type = set(arrayLevel, value)

  setDefault(arrayLevel, 0)
  // Calling fit allows us to select the more appropriate method to do identity/validation/transform or a mix
  // Possibly a better way to pass all params
  // TODO: return with params set
  override def fit(dataset: Dataset[_]): BaseColumnTransformer = {
    getTransformer($(specParams), dataset, $(arrayLevel)).setInputCol(getInputCol).setOutputCol(getOutputCol)
      .setTransformationParams($(specParams))
  }

  override def copy(extra: ParamMap): Estimator[BaseColumnTransformer] = defaultCopy(extra)

  override def transformSchema(schema: StructType): StructType =
    new StructType(schema.filter(_.name.equals($(inputCol))).toArray)
    .add($(outputCol), ColumnTransformEstimator.getType($(specParams)))

}


object ColumnTransformEstimator {

  def getType( m: Mapping ): DataType = {
    (m.source.collection, m.source.fieldType)  match {
      case (true, "complex") => ArrayType( StructType( m.destination.mappings.map(mm => StructField(mm.name, getType( mm)))) )
      case (true, f) => ArrayType(getBaseType(f))
      case (false, f) => getBaseType(f)
      case _ => StringType // Throw?
    }
  }

  def getBaseType( t:  String): DataType = t match {
    case "string" => StringType
    case "long" => LongType
      // TODO: Complete
    case _ => StringType
  }

  def getTransformer( m: Mapping, dataset: Dataset[_], arrayLevel: Int): ColumnTransformer = {

    if( m.source.fieldType != "complex" && ( m.transformations == null || m.transformations.isEmpty ) )
      return withValidation( m, new IdentityColumnTransformer(), if(m.source.collection) arrayLevel+1 else arrayLevel)

    (m.source.collection, m.source.fieldType) match {
      case (false, "complex") if arrayLevel == 0 => createComplexColumnTransformer(m, dataset, arrayLevel)
      case (false, "complex") if arrayLevel > 0 => createArrayComplexColumnTransformer(m, dataset, arrayLevel )
      case (true, "complex") => createArrayComplexColumnTransformer(m, dataset, arrayLevel +1)
      case (true, _) =>  withValidation ( m, new SqlColumnTransformer().setArrayLevel(arrayLevel +1), arrayLevel +1)
      case (_,_) => withValidation ( m, new SqlColumnTransformer().setArrayLevel(arrayLevel), arrayLevel )
    }

  }

  def createComplexColumnTransformer(m: Mapping, dataset: Dataset[_], arrayLevel: Int): AbstractComplexSqlColumnTransformer = {
   val children: Map[String, BaseColumnTransformer ] = m.destination.mappings
      .map( mapping
                => (mapping.source.name, new ColumnTransformEstimator()
          .setSpecParams(mapping)
          .setInputCol(mapping.source.name)
          .setOutputCol(mapping.destination.name).setArrayLevel(arrayLevel).fit(dataset))
    ).toMap

    new ComplexSqlColumnTransformer(children).setArrayLevel(arrayLevel)
  }

  // TODO: Lenses would be great
  def createArrayComplexColumnTransformer(m: Mapping, dataset: Dataset[_], arrayLevel: Int): AbstractComplexSqlColumnTransformer = {
    val children: Map[String, BaseColumnTransformer ] = m.destination.mappings
      .map( mapping
            => (mapping.source.name, new ColumnTransformEstimator()
          .setSpecParams(mapping)
          .setInputCol(mapping.source.name)
          .setOutputCol(mapping.destination.name).setArrayLevel(arrayLevel).fit(dataset))
      ).toMap

    new ComplexArraySqlColumnTransformer(children).setArrayLevel(arrayLevel)
  }

  def withValidation(m: Mapping, t: SqlColumnTransformer, level: Int): ColumnTransformer = {

    val validator = if(m.validations != null && m.validations.nonEmpty) {
      Some(new SqlColumnValidator().setArrayLevel(level)
             .setInputCol(m.destination.name).setOutputCol(m.destination.name)
             .setValidationParams(m.validations)
        )
    }
    else None

    new ValidatedTransformer(t, validator)
  }



}