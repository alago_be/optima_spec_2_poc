package com.boxever.optima.spec.mapper.transform

import com.boxever.labs.spark.optima.spec.Mapping
import org.apache.spark.ml.param.{Param, ParamMap, Params}
import org.apache.spark.ml.{Transformer => mlTransformer}
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.sql.{DataFrame, Dataset}
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.{ArrayType, StructField, StructType}

class SchemaFlattener(val mappings: List[Mapping]) extends mlTransformer with Params {


  override def copy(extra: ParamMap): mlTransformer = defaultCopy(extra)

  override val uid: String = Identifiable.randomUID("SchemaFlattener");

  override def transform(dataset: Dataset[_]): DataFrame = {
    //val mappings: Param[List[Mapping]] = getParam("mappings").asInstanceOf[Param[List[Mapping]]]
    val conversions = SchemaFlattener.schemaConversions(null, mappings)
    val ds = conversions.foldRight(dataset.asInstanceOf[DataFrame])( (c, d) =>  d.withColumn(c.dst, col(c.src)))
    ds.toDF()

  }

  override def transformSchema(schema: StructType): StructType = {
    val conversions = SchemaFlattener.schemaConversions(null, mappings)
    val flatSchema = SchemaFlattener.fullFlattenSchema(schema);
    StructType( schema ++ conversions.map(c => {
      val old: StructField = flatSchema(c.src)
      new StructField(c.dst, old.dataType, old.nullable, old.metadata)
    })
    )
  }
}


object SchemaFlattener {

  case class ColConversion( src: String, dst: String)

  def schemaConversions(prefix: String, mappings: List[Mapping]): List[ColConversion] = {
    mappings.flatMap(m => {
      m.fieldType match {
        case "complex" => schemaConversions(m.source.name, m.destination.mappings)
        case _ => List(ColConversion(
          Seq(prefix, m.source.name).flatMap(Option[String]).mkString("."),
          Seq(prefix, m.destination.name).flatMap(Option[String]).mkString("."))
        )
      }
    }
    )
  }

//  def flattenedSchema(prefix: String, mappings: List[Mapping]): List[Mapping] = {
//    mappings.flatMap(m => {
//      m.fieldType match {
//        case "complex" => flattenedSchema(m.source.name, m.destination.mappings)
//        case _ => List( m.copy(m).source.name  )
//      }
//    }
//    )
//  }


  // Src: https://stackoverflow.com/questions/51374630/how-to-traverse-through-a-schema-in-spark
  def fullFlattenSchema(schema: StructType): Map[String, StructField] = {
    def helper(schema: StructType, prefix: String): Seq[(String, StructField)] = {

      val fullName: String => String = name => if (prefix.isEmpty) name else s"$prefix.$name"

      schema.fields.flatMap
        {
          case f@StructField(name, inner: StructType, _, _) => helper(inner, fullName(name))
          case f@StructField(name, _, _, _) => Seq((fullName(name), f))
          //case f@StructField(name, inner: ArrayType => helper(inner.elementType, fullname(name))

        }

    }

    helper(schema, "").toMap
  }


}
