package com.boxever.optima.spec.mapper.validations

import org.apache.spark.sql.Column

object functions {

  def rlike(c: Column, params: Option[List[String]]): Column = {
    c.rlike(params.get.head)
  }

  def IsUID(c: Column, params: Option[List[String]]): Column = {
    c.rlike("^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$")
  }

  def isIn(c: Column, params: Option[List[String]]): Column = {
    c.isin(params.getOrElse(List()): _*)
  }

}