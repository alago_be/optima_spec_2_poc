package com.boxever.optima.spec.mapper

import com.boxever.labs.spark.optima.spec.Mapping
import org.apache.spark.ml.param.{IntParam, Param, ParamValidators, Params}

package object estimators {

  class MappingParam(parent: Params, name: String, doc: String, isValid: Mapping => Boolean )
    extends Param[Mapping](parent, name, doc, isValid) {

    def this(parent: Params, name: String, doc: String) =
      this(parent, name, doc, _ => true)
  }

  trait HasSpecParams extends Params {

    final val specParams: MappingParam =
      new MappingParam(this, "specParams", "")

    def getSpecParams: Mapping = $(specParams)
  }




}

