package com.boxever.optima.spec.mapper

import com.boxever.labs.spark.optima.spec.Mapping
import com.boxever.optima.spec.mapper.models.HasArrayLevelParams
import org.apache.spark.ml.param.{Param, Params}
import org.apache.spark.sql.Column
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{BooleanType, DoubleType, IntegerType, LongType}


package object transform {

  class TransformationParam(parent: Params, name: String, doc: String, isValid: Mapping => Boolean)
    extends Param[Mapping](parent, name, doc, isValid) {

    def this(parent: Params, name: String, doc: String) =
      this(parent, name, doc, _ => true)
  }

  trait HasTransformationParams extends Params {

    // TODO: receive the list of optima.Function and get inputCol and outputCol from self
    //self: HasInputCol with HasOutputCol =>

    final val transformationParams: TransformationParam =
      new TransformationParam(this, "transformationParams", "")

    def getTransformationParams: Mapping = $(transformationParams)

    def setTransformationParams(value: Mapping): this.type = set(transformationParams, value)

  }

  class SQLTransformationParam(parent: Params, name: String, doc: String, isValid: Column => Boolean)
    extends Param[Column](parent, name, doc, isValid) {

    def this(parent: Params, name: String, doc: String) =
      this(parent, name, doc, _ => true)
  }

  trait HasSQLTransformationParams extends HasTransformationParams with HasArrayLevelParams {

    final val sqlTransformationParams: SQLTransformationParam =
      new SQLTransformationParam(this, "SQLTransformationParam", "")

    def getSQLTransformationParams: Column = $(sqlTransformationParams)

    override def setTransformationParams(value: Mapping): this.type = {
      set(transformationParams, value)
      val query: Column =  if($(arrayLevel) == 0) {
        createQuery(value)
      } else {
        createArrayQuery(value, $(arrayLevel))
      }
      set(sqlTransformationParams, query)
    }

  }

  def createQuery(mapping: Mapping): Column = {
    val inputCol = mapping.source.name

    mapping.transformations.foldLeft(col(inputCol))((acc, t) => {
      SQLTransformers(t.name)(acc, t.params.getOrElse(List()))
    })
  }

  // this works in 3.0 adex.withColumn("k", transform( col("a.b.c"),  transform(_, _+1) )).show()
  // With spark 3. we have transform as functions method
  // 2.4 limits us to pass it as expr, and do this of passing the lambda as text....
  def createArrayQuery(mapping: Mapping, arrayLevel: Int): Column = {
    val inputCol = mapping.source.name

    val q = createQuery(mapping).expr.sql.replace( inputCol, "x" )

    val t = "x -> transform(x,"
    val p = ")"

    val r = arrayLevel-1
    val repeatedQuery = s"transform($inputCol, ${t*r}  x -> $q) ${p *r}"
    // Examples:
    //val a1 = s"transform($inputCol, x -> $q)"
    //val a2 = s"transform($inputCol, x -> transform(x,  x ->  $q) )"
    //val a3 = s"transform($inputCol, x -> transform(x,  x -> transform(x, x->  $q) ))"

    expr(repeatedQuery)
  }



 val SQLTransformers: Map[String, (Column, List[String]) => Column]= Map(
   ("setStringIfNull", ColumnTransformations.setStringIfNull _),
   ("setLongIfNull", ColumnTransformations.setLongIfNull _),
   ("toLowerCase",ColumnTransformations.toLowerCase _),
   ("trim", ColumnTransformations.trim _)
 )


  object ColumnTransformations {

    def setStringIfNull(column: Column, params: List[String]): Column = {
      coalesce(column, lit(params.head))
    }

    def setBooleanIfNull(column: Column, params: List[String]): Column = {
      coalesce(column, typedLit[Boolean](params.head.toBoolean))
    }

    def setIntIfNull(column: Column, params: List[String]): Column = {
      coalesce(column, typedLit[Int](params.head.toInt))
    }

    def setLongIfNull(column: Column, params: List[String]): Column = {
      coalesce(column, typedLit[Long](params.head.toLong))
    }

    def setDoubleIfNull(column: Column, params: List[String]): Column = {
      coalesce(column, typedLit[Double](params.head.toDouble))
    }

    def toLowerCase(column: Column, params: List[String]): Column = {
      lower(column)
    }

    def toUpperCase(column: Column, params: List[String]): Column = {
      upper(column)
    }

    def trim(column: Column, params: List[String]): Column = {
      org.apache.spark.sql.functions.trim(column)
    }

    def replaceOneOf(column: Column, params: List[String]): Column = {
      val replaceOneOf: List[String] = params.slice(0, params.size - 1)
      val replaceWithValue: String = params.last
      replaceOneOf.foldLeft(column)((c, v) => regexp_replace(c, v, replaceWithValue))
    }

    //
    //    def castLongToTimestampInt64(value: Long): Option[Timestamp] = {
    //      Option.apply(new Timestamp(value))
    //    }
    //
    //    def castStringToTimestampInt64(value: String): Option[Timestamp] = {
    //      try {
    //        return Option.apply(new Timestamp(value.toLong))
    //      } catch {
    //        case _: NumberFormatException =>
    //          return Option.apply(new Timestamp(ISODateTimeFormat.dateOptionalTimeParser().parseDateTime(value).getMillis))
    //      }
    //    }
    //
    //    def castLongToDate(value: Long): Option[Date] = {
    //      Option.apply(new Date(value))
    //    }
    //
    //    def castStringToDateWithPattern(value: String, pattern: String): Option[Date] = {
    //      Option.apply(new Date(SafeSimpleDateFormat.get(pattern).parse(value).getTime))
    //    }
    //
    //    def castStringToDate(value: String): Option[Date] = {
    //      try {
    //        return Option.apply(new Date(value.toLong))
    //      } catch {
    //        case _: NumberFormatException =>
    //          return Option.apply(new Date(ISODateTimeFormat.dateOptionalTimeParser().parseDateTime(value).getMillis))
    //      }
    //    }
    //
    //    def castStringToInt(value: String): Option[Int] = {
    //      Option.apply(value.toInt)
    //    }
    //
    //    def castLongToInt(value: Long): Option[Int] = {
    //      Option.apply(value.toInt)
    //    }
    //
    //    object SafeSimpleDateFormat extends ThreadLocal[util.HashMap[String, SimpleDateFormat]] {
    //      override def initialValue = {
    //        new util.HashMap[String, SimpleDateFormat]()
    //      }
    //
    //      def get(pattern: String): SimpleDateFormat = {
    //        val map: util.HashMap[String, SimpleDateFormat] = get()
    //        if (!map.containsKey(pattern)) {
    //          map.put(pattern, new SimpleDateFormat(pattern));
    //        }
    //        return map.get(pattern)
    //      }
    //    }
    //
    //    def formatDate(value: Any, params: List[String]): Option[String] = {
    //      if (value != null && value.isInstanceOf[Date]) Option.apply(SafeSimpleDateFormat.get(params.head).format(value)) else Option.empty
    //    }

  }
  }
