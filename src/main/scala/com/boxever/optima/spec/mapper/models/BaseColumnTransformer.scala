package com.boxever.optima.spec.mapper.models

import org.apache.spark.ml.Model
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.ml.param.shared.{HasInputCol, HasOutputCol}

import org.apache.spark.sql.types.{StringType, StructType}


trait BaseColumnTransformer extends Model[BaseColumnTransformer]
  with HasInputCol with HasOutputCol {

  def setInputCol(value: String): this.type = set(inputCol, value)
  setDefault(inputCol -> "")
  def setOutputCol(value: String): this.type = set(outputCol, value)
  setDefault(outputCol -> "")

  override def transformSchema(schema: StructType): StructType =
    new StructType(schema.filter(_.name.equals($(inputCol))).toArray)
      .add($(outputCol), StringType)

  override def copy(extra: ParamMap): BaseColumnTransformer = defaultCopy(extra)
}
