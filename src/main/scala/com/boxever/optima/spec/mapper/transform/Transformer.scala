package com.boxever.optima.spec.mapper.transform

import java.util.UUID

import com.boxever.labs.spark.optima.spec.Mapping
import com.boxever.optima.spec.mapper.models.{BaseColumnTransformer, HasArrayLevelParams}
import com.boxever.optima.spec.mapper.validations.{ColumnValidator, Validator}
import org.apache.spark.ml.param.shared.HasOutputCol
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.sql.{Column, DataFrame, Dataset}
import org.apache.spark.sql.functions.{arrays_zip, col, expr, struct}
import org.apache.spark.sql.types.{StringType, StructField, StructType}

import scala.annotation.tailrec

trait Transformer {
  // Since we will mix it under ml.Transformers, transform is taken
  def transformValue(dataset: Dataset[_]): Dataset[_]
}


trait ColumnTransformer extends BaseColumnTransformer with HasTransformationParams{
  self: Transformer =>

  override def transform(dataset: Dataset[_]): DataFrame = {
    transformValue(dataset).toDF()
  }
}

trait ColumnTransformerAndValidator extends ColumnTransformer with ColumnValidator {
  self:  Transformer with Validator =>

  override def transform(dataset: Dataset[_]): DataFrame = {
    val df = transformValue(dataset);
    validate(df).toDF()
  }

}


// Equals to No-OP
class IdentityColumnTransformer(override val uid: String) extends SqlColumnTransformer with Transformer{

  def this() = this(Identifiable.randomUID("IdentityColumnTransformer"))

  override def transformSchema(schema: StructType): StructType = {
    StructType(schema.map( s => if(s.name == $(inputCol)) {
      StructField($(outputCol),s.dataType, s.nullable, s.metadata)
    }
    else s
    )
    )
  }
  override def transformValue(dataset: Dataset[_]): Dataset[_] = dataset.withColumnRenamed($(inputCol), $(outputCol))

  override def setTransformationParams(value: Mapping): this.type = {
    //set(transformationParams, value)

    this
  }
}

class SqlColumnTransformer(override val uid: String) extends ColumnTransformer with Transformer
  with HasSQLTransformationParams with HasArrayLevelParams {

  def this() = this(Identifiable.randomUID("SqlColumnTransformer"))

  def transformValue(dataset: Dataset[_]): Dataset[_] = {
    if( Some($(transformationParams).transformations).getOrElse(List()).nonEmpty) {
      val func: Column = $(sqlTransformationParams)
      val transformed = dataset.withColumn($(outputCol), func)

      if( $(outputCol) == $(inputCol) ) transformed else transformed.drop($(inputCol))

    } else {
      dataset.withColumnRenamed($(inputCol), $(outputCol))
    }
  }

  def setArrayLevel(value: Int): this.type = set(arrayLevel, value)

  setDefault(arrayLevel, 0)

}

class ValidatedTransformer[T <: SqlColumnTransformer, V <: ColumnValidator](transformer: T, validator: Option[V], override val uid: String)
  extends SqlColumnTransformer with HasOutputCol{

  def this(transformer: T, validator: Option[V] ) = this(transformer, validator, Identifiable.randomUID("ValidatedTransformer"))

  override def transformValue(dataset: Dataset[_]): Dataset[_] = {
    val ret = transformer.transformValue(dataset)
    validator match {
      case Some(v) => v.transform(ret)
      case None => ret
    }
  }

  override def setTransformationParams(value: Mapping): ValidatedTransformer.this.type = {
    transformer.setTransformationParams(value)
    this
  }

  override def setInputCol(value: String): ValidatedTransformer.this.type = {
    transformer.setInputCol(value)
    super.setInputCol(value)
  }

  override def setOutputCol(value: String): ValidatedTransformer.this.type = {
    transformer.setOutputCol(value)
    super.setOutputCol(value)
  }

}

//trait ArrayTransformer extends ColumnTransformer with Transformer with HasSQLTransformationParams with HasArrayLevelParams{
//  override def setTransformationParams(value: Mapping): this.type = {
//    set(transformationParams, value)
//    val query: Column = createArrayQuery(value, $(arrayLevel))
//    set(sqlTransformationParams, query)
//  }
//}
//
//
//
//class ArraySqlColumnTransformer(override val uid: String) extends SqlColumnTransformer with ArrayTransformer {
//
//  def this() = this(Identifiable.randomUID("ArraySqlColumnTransformer"))
//
//  // This works
//  //df.withColumn( "new", expr(s"""transform( value, v -> ${f(col("v"))} )""") ).show
//
//}

// TODO: this to handle complex columns, in some way like this:
// val x = myjson.withColumn( "id",myjson("identifiers.id") )
// .withColumn( "provider",myjson("identifiers.provider") )
// .withColumn("identifiers",arrays_zip(col("id"), col("provider")))
// .drop("id").drop("provider")
// .printSchema

// val x = myjson.withColumn( "id",myjson("identifiers.id") )
// .withColumn( "provider",transform(col("identifiers.provider"), upper _ ) )
// .withColumn("identifiers",arrays_zip(col("id"), col("provider")))
// .drop("id")
// .drop("provider")
// .show

// Only problem is that arrays_zip doesnt understand aliases, so we must put the column to root level before
// zipping it, to get the desierd name, it oculd override a present column, a guard ( wrapping the expression
// with renaming of root column if exists needs to be added.
// Catalyst will optimize that making as this didnt exist at all


abstract class AbstractComplexSqlColumnTransformer(columns: Map[String, BaseColumnTransformer]) extends SqlColumnTransformer {

  def merge_method(c: Column*): Column
  override val uid: String
  // TODO: this will have a bug when root and complex column share a name, fix it by wrapping in a rename
  override def transformValue(dataset: Dataset[_]): Dataset[_] = {
    val me: String =  $(transformationParams).source.name
    val newMe: String = $(transformationParams).destination.name
    val children: Seq[String] = columns.keys.toSeq
    val newChildren: Seq[String] = columns.values.map(_.getOutputCol).toSeq

    val root_cols = dataset.columns.intersect(children ++ newChildren)

    val uid: String = UUID.randomUUID().toString
    val datasetRenamed = AbstractComplexSqlColumnTransformer.renameRootColumns(dataset, root_cols, uid)

    // Transform children
    var transformedDs =
       columns.values
         .foldLeft(datasetRenamed)(
           (cur, transformer) =>
             transformer.transform(
               AbstractComplexSqlColumnTransformer.colGetter($(arrayLevel), cur, me, transformer.getInputCol)
               )
             )

        // merge to struct
        .withColumn(newMe, merge_method(newChildren.map(col): _*))
        // Drop temp columns
        .drop(newChildren ++ children:_*)

    // arrays_zip doesnt get the right name when using lambdas, it uses number indexes
    // So we can cast the struct to one fixed
    if($(arrayLevel )> 1) {
      val column = transformedDs(newMe)
      val to : String = AbstractComplexSqlColumnTransformer.cast(column, newChildren:_*)
      transformedDs = transformedDs.withColumn(newMe, col(newMe).cast(to))
    }

    AbstractComplexSqlColumnTransformer.undoRenameRootColumns(transformedDs, root_cols, uid)

  }

  override def setTransformationParams(value: Mapping): this.type = {
    set(transformationParams, value)
  }

}

object AbstractComplexSqlColumnTransformer {


  private def renameRootColumns(dataset: Dataset[_], columns: Seq[String], prefix: String): DataFrame = {
    columns.foldLeft(dataset.toDF)( (cur, c) =>
      cur.withColumnRenamed(c, prefix + c)).toDF
  }

  private def undoRenameRootColumns(dataset: Dataset[_], columns: Seq[String], prefix: String): DataFrame = {
    columns.foldLeft(dataset.toDF)( (cur, c) =>
                                 cur.withColumnRenamed(prefix + c, c)).toDF
  }

  private def colGetter( arrayLevel: Int, dataFrame: Dataset[_], me: String, column: String ): Dataset[_] = {
    if(arrayLevel < 2) {
       dataFrame.withColumn(column, col(me + "." + column))
    } else {
      val t = "x -> transform(x,"
      val p = ")"

      val r = arrayLevel-1
      val repeatedQuery = s"transform($me, ${t*r}  x -> x.$column) ${p *r}"

      dataFrame.withColumn(column, expr(repeatedQuery ))
    }
  }

  def cast(c: Column, columns: String*) = {
    def rep(schema: String, colWithIndex: (String, Int)): String =
      schema.replace(colWithIndex._2.toString, colWithIndex._1)

    columns.zipWithIndex.foldLeft(c.expr.dataType.catalogString)(rep)
  }
}


class ComplexSqlColumnTransformer(columns: Map[String, BaseColumnTransformer], override val uid: String) extends
 AbstractComplexSqlColumnTransformer(columns) {

  def this(columns: Map[String, BaseColumnTransformer]) =
    this(columns, Identifiable.randomUID("ComplexSqlColumnTransformer"))

  def merge_method(c: Column*): Column = struct(c:_*)
}

class ComplexArraySqlColumnTransformer(columns: Map[String, BaseColumnTransformer], override val uid: String) extends
  AbstractComplexSqlColumnTransformer(columns) {

  def this(columns: Map[String, BaseColumnTransformer]) =
    this(columns, Identifiable.randomUID("ComplexArraySqlColumnTransformer"))

  //def merge_method(c: Column*): Column = arrays_zip(c:_*)

  def merge_method(c: Column*): Column = {
    if($(arrayLevel) <2) {
      arrays_zip(c:_*)
    } else {
      // level = 2
      //expr(s"""transform( arrays_zip(${c.mkString(", ")}) , i -> arrays_zip($cols))""")
      // level = 3
      // transform(  ,  transform( i, i=> arrays_zip(i("0"),i("1") ) ) ))
      // level = 4
      // transform(
      //     transform(
      //          transform(
      //              arrays_zip( col("level4"), col("level4b") ) ,
      //                    i => arrays_zip(i("level4"),i("level4b"))) ,
      //           i => transform( i
      //                    , i=> arrays_zip(i("0"),i("1") ) ) ),
      //        i => transform( i, i => transform(i, i=> arrays_zip(i("0"),i("1") ) ) ))))


      val e = get_array_zip($(arrayLevel), c:_*)
      println(e)
      expr(e)
    }

  }

  // TODO: make tail rec if possible
  private final def get_array_zip(r: Int, c: Column*): String = {
    if( r == 2 ) {
      val cols = c.map(t => s"""i.$t """).mkString(", ")
      s"""transform( arrays_zip(${c.mkString(", ")}) , i -> arrays_zip($cols))"""
    } else {
      // arrays_zip changes columns name to numbers in higher level functions
      val cols = (0 until c.size).map(t => s"""i.`$t` """).mkString(", ")
      val inner = get_array_zip( r-1, c:_* )
      val t = "transform( i, i-> "
      val p = ")"
      val times = r-2
      s"""transform( $inner , i -> ${t*times} arrays_zip($cols) ) ${p*times}"""
    }
  }



}