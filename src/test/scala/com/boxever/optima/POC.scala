package com.boxever.optima

import com.boxever.labs.spark.optima.complexstructure.ComplexStructureDestinationSchema.getSourceSchema
import com.boxever.labs.spark.optima.spec.OptimaSpecReader
import com.boxever.optima.spec.mapper.transform.DataExtensionTransformer
import org.apache.spark.sql.DataFrame
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class POC extends AnyFlatSpec with Matchers with SparkSessionTestWrapper{

  import spark.implicits._

  "TransformPOC" should "return process data" in {

    val mapps = getClass.getResource("/mappings/guest_simple/guests0001.yml").toURI.getPath;
    val df = getClass.getResource("/mappings/guest_simple/guests0001.json").toURI.getPath;

    val mappings = OptimaSpecReader.read(mapps, Map())

    val startTime = System.nanoTime

    val dexSchema = getSourceSchema(mappings.mappings)

    val schema = getSourceSchema(mappings.mappings)
    val sourceDF = spark.read.schema(schema).json(df)

    sourceDF.show()
    TransformPOC.run( mappings, () => sourceDF, out )


    val endTime = System.nanoTime
    val timeElapsed = endTime - startTime


    println("========="+timeElapsed)
  }

  def out(x: DataFrame, y: DataFrame, z: Option[DataFrame]): Unit = {
    println(x.count() +" - " + y.count())

    x.explain(true)
    x.show(false)
    x.toJSON.show(false)
    y.show()
    //println(y.select("validationErrors").collect.mkString(","))
  }
  //18844985948
  //19093675253


//  "TransformPOC" should "flatten events data" in {
//
//    val mapps = getClass.getResource("/mappings/events_date_transformation/events.yml").toURI.getPath;
//    val df = getClass.getResource("/mappings/events_date_transformation/events.json").toURI.getPath;
//
//    val mappings = OptimaSpecReader.read(mapps, Map())
//
//    val startTime = System.nanoTime
//
//    val schema = getSourceSchema(mappings.mappings)
//    //val sourceDF = spark.read.schema(schema).json(df)
//
//    //TransformPOC.run( mappings, () => sourceDF, out )
//    val flattenedSchema = SchemaFlattener.fullFlattenSchema(schema)
//    println(flattenedSchema.mkString);
//
//    val endTime = System.nanoTime
//    val timeElapsed = endTime - startTime
//
//
//    println("========="+timeElapsed)
//  }


  "TransformPOC" should "return flattened data" in {

    val mapps = getClass.getResource("/mappings/guest_collection/guests0001.yml").toURI.getPath;
    val df = getClass.getResource("/mappings/guest_collection/guests0001.json").toURI.getPath;

    val mappings = OptimaSpecReader.read(mapps, Map())

    val startTime = System.nanoTime

    val schema = getSourceSchema(mappings.mappings)
    val sourceDF = spark.read.schema(schema).json(df)

    TransformPOC.run( mappings, () => sourceDF, out )


    val endTime = System.nanoTime
    val timeElapsed = endTime - startTime


    println("========="+timeElapsed)
  }

  "TransformPOC" should "return rename data" in {

    val mapps = getClass.getResource("/mappings/guest_twitter/guests0001.yml").toURI.getPath;
    val df = getClass.getResource("/mappings/guest_twitter/guests0001.json").toURI.getPath;

    val mappings = OptimaSpecReader.read(mapps, Map())

    val startTime = System.nanoTime

    val schema = getSourceSchema(mappings.mappings)
    val sourceDF = spark.read.schema(schema).json(df)

    TransformPOC.run( mappings, () => sourceDF, out )


    val endTime = System.nanoTime
    val timeElapsed = endTime - startTime


    println("========="+timeElapsed)
  }


  "TransformPOC" should "return trim array" in {

    val mapps = getClass.getResource("/mappings/trimStringCollectionComp/guests0001.yml").toURI.getPath;
    val df = getClass.getResource("/mappings/trimStringCollectionComp/guests0001.json").toURI.getPath;

    val mappings = OptimaSpecReader.read(mapps, Map())

    val startTime = System.nanoTime

    val schema = getSourceSchema(mappings.mappings)
    val sourceDF = spark.read.schema(schema).json(df)

    sourceDF.show
    sourceDF.printSchema()
    TransformPOC.run( mappings, () => sourceDF, out )


    val endTime = System.nanoTime
    val timeElapsed = endTime - startTime


    println("========="+timeElapsed)
  }


  "TransformPOC" should "set long if null" in {

    val mapps = getClass.getResource("/mappings/coalesceLong/guests0001.yml").toURI.getPath;
    val df = getClass.getResource("/mappings/coalesceLong/guests0003.json").toURI.getPath;

    val mappings = OptimaSpecReader.read(mapps, Map())

    val startTime = System.nanoTime

    val schema = getSourceSchema(mappings.mappings)
    val sourceDF = spark.read.schema(schema).json(df)

    sourceDF.show
    sourceDF.printSchema()
    TransformPOC.run( mappings, () => sourceDF, out )


    val endTime = System.nanoTime
    val timeElapsed = endTime - startTime


    println("========="+timeElapsed)
  }

  "TransformPOC" should "flatten dataextensions" in {

    val mapps = getClass.getResource("/mappings/guest_data_extensions/guests0001.yml").toURI.getPath
    val df = getClass.getResource("/mappings/guest_data_extensions/guests0001.json").toURI.getPath

    val mapping = OptimaSpecReader.read(mapps, Map()).mappings.filter(_.name == "dataExtensions").head

    val startTime = System.nanoTime

    //val schema = getSourceSchema(mappings.mappings)
    val sourceDF = spark.read.json(df)

    sourceDF.show
    sourceDF.printSchema()
    val dexT = new DataExtensionTransformer().setSpecParams(mapping)

    val transformed = dexT.transform(sourceDF)

    transformed.explain
    transformed.count
    transformed.printSchema

    transformed.toJSON.show(false)

    val endTime = System.nanoTime
    val timeElapsed = endTime - startTime


    println("========="+timeElapsed)
  }

  "TransformPOC" should "validate DataExtensions" in {

    val mapps = getClass.getResource("/mappings/guest_data_extensions/guests0001cm.yml").toURI.getPath
    val df = getClass.getResource("/mappings/guest_data_extensions/guests0003.json.gz").toURI.getPath

    val mappings = OptimaSpecReader.read(mapps, Map())

    val startTime = System.nanoTime

    val schema = getSourceSchema(mappings.mappings)
    val sourceDF = spark.read.schema(schema).json(df)

    sourceDF.show
    sourceDF.printSchema()
    TransformPOC.run( mappings, () => sourceDF, out )


    val endTime = System.nanoTime
    val timeElapsed = endTime - startTime


    println("========="+timeElapsed)
  }

  "TransformPOC" should "more levels" in {

    val mapps = getClass.getResource("/mappings/trimStringCollectionComp/guests0001cm.yml").toURI.getPath
    val df = getClass.getResource("/mappings/trimStringCollectionComp/guests0001.json").toURI.getPath

    val mappings = OptimaSpecReader.read(mapps, Map())

    val startTime = System.nanoTime

    val schema = getSourceSchema(mappings.mappings)
    val sourceDF = spark.read.schema(schema).json(df)

    sourceDF.show
    sourceDF.printSchema()
    TransformPOC.run( mappings, () => sourceDF, out )


    val endTime = System.nanoTime
    val timeElapsed = endTime - startTime


    println("========="+timeElapsed)
  }


  "TransformPOC" should "sample validation" in {

    val mapps = getClass.getResource("/mappings/sampleValidation/guests0001.yml").toURI.getPath;
    val df = getClass.getResource("/mappings/sampleValidation/guests0001.json").toURI.getPath;

    val mappings = OptimaSpecReader.read(mapps, Map())

    val startTime = System.nanoTime

    val schema = getSourceSchema(mappings.mappings)
    val sourceDF = spark.read.schema(schema).json(df)

    sourceDF.show
    sourceDF.printSchema()
    TransformPOC.run( mappings, () => sourceDF, out )


    val endTime = System.nanoTime
    val timeElapsed = endTime - startTime


    println("========="+timeElapsed)
  }

}
