package com.boxever.optima

import com.boxever.labs.spark.optima.complexstructure.ComplexStructureDestinationSchema.getSourceSchema
import com.boxever.labs.spark.optima.complexstructure.ComplexStructureMapper
import com.boxever.labs.spark.optima.spec.OptimaSpecReader
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types.StructType
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class CM extends AnyFlatSpec with Matchers with SparkSessionTestWrapper{

  import spark.implicits._


  "ComplexStructureMapper" should "return process data" in {
    val mapps = getClass.getResource("/mappings/guest_collection/guests0001_optima.yml").toURI.getPath;
    val df = getClass.getResource("/mappings/guest_collection/guests0001.json").toURI.getPath;

    val mappings = OptimaSpecReader.read(mapps, Map())

    val startTime = System.nanoTime
    val schema = getSourceSchema(mappings.mappings)
    val sourceDF = spark.read.schema(schema).json(df)

    ComplexStructureMapper.run(mappings, () => sourceDF, out )


    val endTime = System.nanoTime
    val timeElapsed = endTime - startTime

    println("========="+timeElapsed)

  }

  //64679174778
  //62279641589
  //61726244925
  //70867456683
  //70883176085
  //81743828687
  def out(x: DataFrame, y: DataFrame, z: Option[DataFrame]): Unit = {
    x.explain(true)
    println(x.count() +" - " + y.count())
    x.show(false)
    x.toJSON.show(false)
    y.show()
  }

  "CM" should "return trim array" in {

    val mapps = getClass.getResource("/mappings/trimStringCollectionComp/guests0001cm.yml").toURI.getPath;
    val df = getClass.getResource("/mappings/trimStringCollectionComp/guests0003.json").toURI.getPath;

    val mappings = OptimaSpecReader.read(mapps, Map())

    val startTime = System.nanoTime

    val schema = getSourceSchema(mappings.mappings)
    val sourceDF = spark.read.schema(schema).json(df)

    sourceDF.show
    sourceDF.printSchema()
    ComplexStructureMapper.run(mappings, () => sourceDF, out )


    val endTime = System.nanoTime
    val timeElapsed = endTime - startTime


    println("========="+timeElapsed)
  }


  "CM" should "validate DataExensions" in {

    val mapps = getClass.getResource("/mappings/guest_data_extensions/guests0001cm.yml").toURI.getPath
    val df = getClass.getResource("/mappings/guest_data_extensions/guests0003.json").toURI.getPath

    val mappings = OptimaSpecReader.read(mapps, Map())

    val startTime = System.nanoTime

    val schema = getSourceSchema(mappings.mappings)
    val sourceDF = spark.read.schema(schema).json(df)

    sourceDF.show
    sourceDF.printSchema()
    ComplexStructureMapper.run(mappings, () => sourceDF, out )


    val endTime = System.nanoTime
    val timeElapsed = endTime - startTime


    println("========="+timeElapsed)
  }
}
