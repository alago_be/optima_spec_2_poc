package com.boxever.optima

import com.boxever.labs.spark.optima.spec.{OptimaSpec, OptimaSpecReader}
import com.boxever.optima.spec.mapper.transform.SchemaFlattener
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ColConversionFlattener extends AnyFlatSpec with Matchers {

    "TransformPOC.schemaConversions" should "return a list of conversions" in {

      val mapps = getClass.getResource("/mappings/sample_mappings.yml").toURI.getPath;
      val mappings : OptimaSpec = OptimaSpecReader.read(mapps, Map())


      val colConversions = SchemaFlattener.schemaConversions(null, mappings.mappings)

      println(colConversions.mkString("[",",","]"))
    }



}
