import sbt._
import Keys._

object Resolvers {

  lazy val localMaven =  "Local Maven Repository" at "file://"+Path.userHome+"/.m2/repository"
  lazy val boxSnapshots =  "Boxever Snapshots" at "https://nexus.boxever.com/nexus/content/repositories/boxever-snapshots"
  lazy val boxReleases =  "Boxever Releases" at "https://nexus.boxever.com/nexus/content/repositories/boxever-releases"
  lazy val binTray =  "Bintray sbt plugin releases" at "https://dl.bintray.com/sbt/sbt-plugin-releases/"
  lazy val jcenter =  Resolver.jcenterRepo
  lazy val defaultMaven =  DefaultMavenRepository

  lazy val allResolvers = Seq(localMaven, boxSnapshots, boxReleases, binTray, jcenter, defaultMaven)

  lazy val boxCredentials = Credentials(Path.userHome / ".ivy2" / ".credentials")
}