import Dependencies._
import Resolvers._

//ThisBuild / scalaVersion     := "2.12.11"
ThisBuild / scalaVersion     := "2.11.12"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

// To have transform function and use sql instead of udf, but udfs already improve performance a lot
//lazy val sparkVersion = "3.0.0"

lazy val sparkVersion = "2.4.0"
lazy val root = (project in file("."))
  .settings(
    name := "Optima Specs Engine 2.0",
    scalacOptions in (Compile,doc) := Seq("-diagrams", "-diagrams-debug"),
    libraryDependencies ++= Seq(
      /*(*/"com.boxever" % "boxever-optima-spec-engine" % "0.0.9"/*).exclude("com.fasterxml.jackson.module", "jackson-module-scala_2.11")*/,
      "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
      "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
      "org.apache.spark" %% "spark-mllib" % sparkVersion % "provided",
      scalaTest % Test
    ),
    resolvers ++= allResolvers,
    credentials ++= Seq(boxCredentials),
    scalacOptions ++= Seq(
      "-language:higherKinds"
    )//,
    //assemblyShadeRules in assembly := Seq(
    //  ShadeRule.rename("com.fasterxml.**" -> "shadeio.@1").inAll
    //)

  )



// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
